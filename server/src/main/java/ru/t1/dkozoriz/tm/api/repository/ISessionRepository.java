package ru.t1.dkozoriz.tm.api.repository;

import ru.t1.dkozoriz.tm.model.Session;

public interface ISessionRepository extends IUserOwnedRepository<Session> {

}