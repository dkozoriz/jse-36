package ru.t1.dkozoriz.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.api.endpoint.*;
import ru.t1.dkozoriz.tm.api.repository.ISessionRepository;
import ru.t1.dkozoriz.tm.api.repository.IUserRepository;
import ru.t1.dkozoriz.tm.api.repository.business.IProjectRepository;
import ru.t1.dkozoriz.tm.api.repository.business.ITaskRepository;
import ru.t1.dkozoriz.tm.api.service.*;
import ru.t1.dkozoriz.tm.api.service.business.IProjectService;
import ru.t1.dkozoriz.tm.api.service.business.ITaskService;
import ru.t1.dkozoriz.tm.endpoint.*;
import ru.t1.dkozoriz.tm.enumerated.Role;
import ru.t1.dkozoriz.tm.enumerated.Status;
import ru.t1.dkozoriz.tm.model.User;
import ru.t1.dkozoriz.tm.model.business.Project;
import ru.t1.dkozoriz.tm.repository.SessionRepository;
import ru.t1.dkozoriz.tm.repository.UserRepository;
import ru.t1.dkozoriz.tm.repository.business.ProjectRepository;
import ru.t1.dkozoriz.tm.repository.business.TaskRepository;
import ru.t1.dkozoriz.tm.service.*;
import ru.t1.dkozoriz.tm.service.business.ProjectService;
import ru.t1.dkozoriz.tm.service.business.TaskService;
import ru.t1.dkozoriz.tm.util.SystemUtil;
import ru.t1.dkozoriz.tm.util.TerminalUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@NoArgsConstructor
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService(propertyService);

    @Getter
    @NotNull
    private final IUserService userService = new UserService(userRepository, projectRepository, taskRepository, propertyService);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(this);

    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService(this);

    @Getter
    @NotNull
    private final ISessionService sessionService = new SessionService(sessionRepository);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final IAuthEndpoint authEndpoint = new AuthEndpoint(this);

    @NotNull
    private final Backup backup = new Backup(this);

    {
        registry(systemEndpoint);
        registry(domainEndpoint);
        registry(userEndpoint);
        registry(authEndpoint);
        registry(projectEndpoint);
        registry(taskEndpoint);
    }

    private void initBackup() {
        backup.start();
    }


    private void processCommand(@Nullable final String command) {
        processCommand(command, true);
    }

    public void processCommand(@Nullable final String command, boolean checkRoles) {
        if (command == null || command.isEmpty()) return;
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = getPropertyService().getServerHost();
        @NotNull final String port = getPropertyService().getServerPort().toString();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    private void initDemoData() {
        @NotNull final User user1 = userService.create("u", "p", "user1@user");
        @NotNull final User user2 = userService.create("user2", "password2", "user2@user");
        @NotNull final User admin = userService.create("admin", "admin", Role.ADMIN);

        @NotNull final Project project1 = new Project("Project1", Status.IN_PROGRESS);
        @NotNull final Project project2 = new Project("Project2", Status.IN_PROGRESS);
        @NotNull final Project project3 = new Project("Project3", Status.COMPLETED);
        @NotNull final Project project4 = new Project("Project4", Status.COMPLETED);
        @NotNull final Project project5 = new Project("Project5", Status.COMPLETED);
        @NotNull final Project project6 = new Project("Project6", Status.IN_PROGRESS);
        projectRepository.add(user1.getId(), project1);
        projectRepository.add(user1.getId(), project2);
        projectRepository.add(user1.getId(), project3);
        projectRepository.add(user2.getId(), project4);
        projectRepository.add(user2.getId(), project5);
        projectRepository.add(user2.getId(), project6);

        taskService.create(user1.getId(), "task1_1", "description", project1.getId());
        taskService.create(user1.getId(), "task1_2", "description", project1.getId());
        taskService.create(user1.getId(), "task2_1", "description", project2.getId());
        taskService.create(user1.getId(), "task3_1", "description", project3.getId());
        taskService.create(user1.getId(), "task3_2", "description", project3.getId());
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }


    private void prepareStartup() {
        loggerService.info("*** TASK MANAGER SERVER IS STARTED ***");
        initPID();
        initDemoData();
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        initBackup();
    }

    private void prepareShutdown() {
        backup.stop();
        loggerService.info("** TASK MANAGER SERVER IS SHUTTING DOWN **");
    }

    public void run(@Nullable final String... args) {
        prepareStartup();
    }

}